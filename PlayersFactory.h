//
// Created by Vatson on 016 16.01.2021.
//

#ifndef TASK3_PLAYERSFACTORY_H
#define TASK3_PLAYERSFACTORY_H
#include <utility>

#include "includes.h"
#include "IGamer.h"
#include "GamerUser.h"
#include "GamerOptimal.h"
#include "GamerRandom.h"

class PlayersFactory {
friend class IGamer;

public:
    IGamer* createGamer(PlayerType pt, string name, IGameView* view){
        if (pt == PlayerType::ConsoleGamer) return new GamerUser(std::move(name), view);
        if (pt == PlayerType::OptimalGamer) return new GamerOptimal(std::move(name), view);
        if (pt == PlayerType::RandomGamer) return new GamerRandom(std::move(name), view);
        return nullptr;
    };
};


#endif //TASK3_PLAYERSFACTORY_H
