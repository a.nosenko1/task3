//
// Created by Vatson on 005 05.01.2021.
//

#include "Game.h"
#include "PlayersFactory.h"
#include "ConsoleView.h"

void Game::startGame() {
    for (int i = 0; i < parameters.countRounds; ++i) {
        setupPlayersShips();
        isPlayersSwitched = false;
        while (gameStatus != GameStatus::gameOver) {
            if (currentPlayer->getPlayerType() == PlayerType::ConsoleGamer or parameters.gameMod == GameMod::botVSbot) {
                view->draw(currentPlayer->getField());
                if (isPlayersSwitched) {
                    isPlayersSwitched = false;
                    view->showStackMessages();
                }
                view->printMessage("There is a shot from " + currentPlayer->getName() + " now");
            } //else view->clearBuffer(); // мои сообщения не нужны боту - мб не нужно
            bool isGoodShot = currentPlayer->shot(nextPlayer);
            if (!(parameters.gameMod == GameMod::userVSbot and currentPlayer->getPlayerType() != PlayerType::ConsoleGamer)){
                view->draw(currentPlayer->getField());
                view->showResultMessage(); // тут результат выстрела
                pause(DELAY_TIME);
            };
            if (parameters.gameMod == GameMod::userVSbot and currentPlayer->getPlayerType() == PlayerType::ConsoleGamer)
                view->clearBuffer(); // мои сообщения не нужны боту
            if (isGoodShot) {
                if (currentPlayer->getCountKills() == 10) {
                    view->printMessage(currentPlayer->getName() + " won in this round!\n");
                    wins[(currentPlayer->getName()=="player1")?0:1] += 1;
                    showCurrentScore();
                    if (parameters.countRounds-1 == i) {
                        view->printMessage("It was the last round!");
                        return;
                    } else {
                        restart();
                        break; // новый раунд
                    }
                }
                continue; // стреляет тот же
            } else switchPlayers();
        }
    }
}

void Game::setupPlayersShips() {
    currentPlayer->setupShips();
    nextPlayer->setupShips();
}

void Game::switchPlayers() {
    swap(currentPlayer, nextPlayer);
    isPlayersSwitched = true;
}

Game::Game(Parameters parameters) {
    view = new ConsoleView();
    this->parameters = parameters;
    PlayersFactory pf;
    currentPlayer = pf.createGamer(parameters.firstPlayerType, "player1", view);
    nextPlayer = pf.createGamer(parameters.secondPlayerType, "player2", view);
    if (currentPlayer == nullptr or nextPlayer == nullptr) throw runtime_error("Creating players error");
    isPlayersSwitched = false;
    wins[0] = wins[1] = 0;
}

void Game::restart() {
    view->clearBuffer();
    PlayersFactory pf;
    delete currentPlayer;
    delete nextPlayer;
    currentPlayer = pf.createGamer(parameters.firstPlayerType, "player1", view);
    nextPlayer = pf.createGamer(parameters.secondPlayerType, "player2", view);
    if (currentPlayer == nullptr or nextPlayer == nullptr) throw runtime_error("Creating players error");
    isPlayersSwitched = false;
}

void Game::showCurrentScore() {
    view->printMessage("---Current score:---");
    view->printMessage("  player1 - player2");
    view->printMessage("\t" + to_string(wins[0]) + " - " + to_string(wins[1]));
}
