#ifndef TASK3_PARSER_H
#define TASK3_PARSER_H

#include "optionparser.h"

struct Arg : public option::Arg {
    static option::ArgStatus GamerType(const option::Option &option, bool msg) {
        if (option.arg == ((string)"Optimal"))
            return option::ARG_OK;
        if (option.arg == ((string)"Random"))
            return option::ARG_OK;
        if (option.arg == ((string)"User"))
            return option::ARG_OK;
        else {
            cout << "Unknown player type: " << option.arg << endl;
            return option::ARG_ILLEGAL;
        }
    }
    static option::ArgStatus Numeral(const option::Option &option, bool msg) {
        char* endptr = nullptr;
        if (option.arg != nullptr && strtol(option.arg, &endptr, 10)) {};
        if (endptr != option.arg && *endptr == 0) {
            return option::ARG_OK;
        }
        cout << "Bad number of rounds" << endl;
        return option::ARG_ILLEGAL;
    }
};
enum optionIndex { HELP, COUNT, FIRST, SECOND };
const option::Descriptor usage[] = {
        {HELP,    0, "h",  "help",  Arg::None,
                                                                     "-h --help  Player types: Random/User/Optimal"},
        {COUNT,   0, "c", "count",  Arg::Numeral,
                                                                     "-c <num>, --count <num>  number of rounds, default is 1."},
        {FIRST,   0, "f", "first",  Arg::GamerType,
                                                                     "-f <arg>, --first <arg>  first player type, Random/User/Optimal."},
        {SECOND,  0, "s", "second", Arg::GamerType,
                                                                     "-s <arg>, --second <arg> second player type, Random/User/Optimal."},
        {0,       0, nullptr,   nullptr,        nullptr,             nullptr}
};
struct Parameters{
    Parameters(){
        countRounds = 1;
        firstPlayerType = PlayerType::RandomGamer;
        secondPlayerType = PlayerType::RandomGamer;
        gameMod = GameMod::botVSbot;
    };
    explicit Parameters(option::Option* options){
        int countBots = 0, countUsers = 0;
        if (options[COUNT]) {
            countRounds = atoi(options[COUNT].arg);
            if (countRounds <= 0) throw runtime_error("Wrong number of rounds");
        } else countRounds = 1;
        if (options[FIRST]) {
            if (options[FIRST].arg == ((string)"Optimal")){
                firstPlayerType = PlayerType::OptimalGamer;
            }
            else if (options[FIRST].arg == ((string)"Random")){
                firstPlayerType = PlayerType::RandomGamer;
            }
            else if (options[FIRST].arg == ((string)"User")){
                firstPlayerType = PlayerType::ConsoleGamer;
            }
            else throw runtime_error("Unknown player type");
        }

        if (options[SECOND]) {
            if (options[SECOND].arg == ((string)"Optimal")){
                secondPlayerType = PlayerType::OptimalGamer;
            }
            else if (options[SECOND].arg == ((string)"Random")){
                secondPlayerType = PlayerType::RandomGamer;
            }
            else if (options[SECOND].arg == ((string)"User")){
                secondPlayerType = PlayerType::ConsoleGamer;
            }
            else throw runtime_error("Unknown player type");
        }
        if (firstPlayerType == PlayerType::ConsoleGamer) countUsers++;
        if (firstPlayerType != PlayerType::ConsoleGamer) countBots++;
        if (secondPlayerType == PlayerType::ConsoleGamer) countUsers++;
        if (secondPlayerType != PlayerType::ConsoleGamer) countBots++;
        if (countBots == 2) gameMod = GameMod::botVSbot;
        else if (countUsers == 2) gameMod = GameMod::userVSuser;
        else gameMod = GameMod::userVSbot;
    }
    int countRounds = 1;            //(c)
    PlayerType firstPlayerType = PlayerType::RandomGamer;  //(f)
    PlayerType secondPlayerType = PlayerType::RandomGamer;  //(s)
    GameMod gameMod;
};

#endif //TASK3_PARSER_H
