//
// Created by Vatson on 017 17.01.2021.
//

#ifndef TASK3_GAMEROPTIMAL_H
#define TASK3_GAMEROPTIMAL_H
#include "IGamer.h"
#include "includes.h"
enum class TypesOptimalSetup{
    Left = 1,
    Right = 2,
    LeftAndUp = 3,
    LeftAndRight= 4,
};
class GamerOptimal: public IGamer {
public:
    explicit GamerOptimal(string name, IGameView* view);
    ~GamerOptimal() override;
    void setupShips() override;
    bool shot(IGamer* opponent) override;
    ShotReturn receiveShot(Coordinates coordinates) override;

    int getCountKills() const override;
    string getName() const override;
    Field* getField() const override;
    PlayerType getPlayerType() const override;

private:
    Field* field;
    int countKills;
    IGameView* view;
    vector<vector<int>>* cleverRadar;
    vector<Coordinates>* coordDiagonalShot;
    bool isShipDestroying; // два состояния: либо сейчас добиваем, либо следуем тактике
    vector<int>* ships;
    const PlayerType pt = PlayerType::OptimalGamer;
    int countWins; // кол-во побед

    void setupShipsType(TypesOptimalSetup type); // тип расстановки кораблей
    Coordinates findHeavyCoordToShot() const;    // когда добиваем корабль
    Coordinates whereShot() const;               // общая функция куда стрелять дальше
    void setDiagonalCoordinates(vector<Coordinates>* v);
    bool isFourShipsLive() const;
    void setSquareZero(Coordinates coord);
    void acceptGet(Coordinates coord); //сверху, снизу, слева, справа
};


#endif //TASK3_GAMEROPTIMAL_H
