//
// Created by Vatson on 005 05.01.2021.
//

#include "Field.h"
#include "ConsoleView.h"

Field::Field(IGameView* view) {
    ourField = new vector<vector<Cell*>>;
    radar = new vector<vector<Cell*>>;
    for (int i = 0; i < 10; ++i) {
        auto* temp = new vector<Cell*>;
        for (int j = 0; j < 10; ++j) {
            Cell* a = new Cell();
            temp->push_back(a);
        }
        ourField->push_back(*temp);
    }
    for (int i = 0; i < 10; ++i) {
        auto* temp = new vector<Cell*>;
        for (int j = 0; j < 10; ++j) {
            Cell* a = new Cell();
            temp->push_back(a);
        }
        radar->push_back(*temp);
    }
    for (int i = 0; i < 10; ++i) { //так надежнее чем делать пуш в одном цикле
        for (int j = 0; j < 10; ++j) {
            Cell* st = (*ourField)[i][j];
            Cell* st1 = (*radar)[i][j];
            (*st).coord = {i,j};
            (*st1).coord = {i,j};
        }
    }
    ships = new vector<Ship>;
    this->view = view;
}

bool Field::setupShip(Ship ship) {
    //поступает правильный линейный корабль
    for (auto coord: ship.coordinates) { //можно ли ставить корабль по его координатам
        if (!isSetupShipPossible(coord))
            return false;
    }
    for (auto coord: ship.coordinates){
        Cell* tempCell = getCell(coord, TypeField::ourField);
        if (tempCell->status == CellStatus::Ship)
            return false;
        ship.cells.push_back(tempCell);
        tempCell->status = CellStatus::Ship;
    }
    ships->push_back(ship);
    return true;
}

ShotReturn Field::receiveShot(Coordinates coordinates) { //нормальные координаты
    Cell* tempCellPointer = getCell(coordinates, TypeField::ourField);
    ShotReturn sr = makeShot(tempCellPointer);
    return sr;
}

bool Field::isSetupShipPossible(Coordinates coordinates, TypeField tf) const {
    if (!(coordinates.x < 0 or coordinates.y < 0 or coordinates.x > 9 or coordinates.y > 9)){
        Cell* tempCell = getCell(coordinates, TypeField::ourField);
        if (isShipAround(tempCell, TypeField::ourField))
            return false;
    } else return false;
    return true;
}

bool Field::isShipAround(Cell* cs, TypeField tf) const {
    vector<Coordinates> suspicious_cells;
    suspicious_cells.emplace_back(cs->coord.x+1,cs->coord.y);
    suspicious_cells.emplace_back(cs->coord.x-1,cs->coord.y);
    suspicious_cells.emplace_back(cs->coord.x,cs->coord.y+1);
    suspicious_cells.emplace_back(cs->coord.x,cs->coord.y-1);
    suspicious_cells.emplace_back(cs->coord.x-1,cs->coord.y-1);
    suspicious_cells.emplace_back(cs->coord.x+1,cs->coord.y+1);
    suspicious_cells.emplace_back(cs->coord.x+1,cs->coord.y-1);
    suspicious_cells.emplace_back(cs->coord.x-1,cs->coord.y+1);
    for (auto coord:suspicious_cells){
        if (!(coord.x < 0 or coord.y < 0 or coord.x > 9 or coord.y > 9))
            if (getCell(coord, tf)->status == CellStatus::Ship) return true;
    }
    return false;
}

Cell *Field::getCell(Coordinates coordinates, TypeField tf) const {
    if (coordinates.x < 0 or coordinates.y < 0 or coordinates.x > 9 or coordinates.y > 9)
        return nullptr;
    if (tf == TypeField::Radar){
        return (*radar)[coordinates.x][coordinates.y];
    }
    if (tf == TypeField::ourField){
        Cell* st = (*ourField)[coordinates.x][coordinates.y];
        return st;
    }
    return nullptr;
}

ShotReturn Field::makeShot(Cell *cell) {
    ShotReturn sr{};
    Ship *tempShipPointer = whatShip(cell); //mb null
    if (cell->status == CellStatus::Empty) {
        cell->status = CellStatus::Touched;
        sr.shotRes = ShotRes::Miss;
        return sr;
    }
    if (cell->status == CellStatus::Ship) {
        cell->status = CellStatus::Killed;
        if (isShipAlive(tempShipPointer))
            sr.shotRes = ShotRes::Get;
        else {
            sr.shotRes = ShotRes::Kill;
            setCellsAround(tempShipPointer,CellStatus::Touched,TypeField::ourField);
        }
        acceptGetTouched(cell->coord, TypeField::ourField);
        sr.ship = tempShipPointer;
        return sr;
    }
    if (cell->status == CellStatus::Touched or cell->status == CellStatus::Killed){
        sr.shotRes = ShotRes::Bad;
        return sr;
    }
    return sr;
}

Ship *Field::whatShip(Cell *cell) const {
    for (auto & ship : *ships){
        for (Coordinates coord:(ship.coordinates)) {
            if (coord == cell->coord)
                return &ship;
        }
    }
    return nullptr;
}

bool Field::isShipAlive(Ship *ship) const {
    for (Cell* cell: ship->cells){
        if (cell->status == CellStatus::Ship)
            return true;
    }
//    int i = 0; // удаляем корабль
//    for (; i < ships->size(); ++i) {
//         if ((*ships)[i] == *ship)
//             break;
//    }
//    ships->erase(ships->begin() + i);
//    ships->shrink_to_fit();

    return false;
}

void Field::setCellsAround(Ship *ship, CellStatus cs, TypeField tf) {
    for (Coordinates coord:ship->coordinates){
        setCellsSquare(getCell(coord,tf), cs, tf);
    }
}

void Field::setCellsSquare(Cell *cell, CellStatus cst, TypeField tf) {
    vector<Coordinates> suspicious_cells;
    suspicious_cells.push_back({cell->coord.x+1,cell->coord.y});
    suspicious_cells.push_back({cell->coord.x-1,cell->coord.y});
    suspicious_cells.push_back({cell->coord.x,cell->coord.y+1});
    suspicious_cells.push_back({cell->coord.x,cell->coord.y-1});
    suspicious_cells.push_back({cell->coord.x-1,cell->coord.y-1});
    suspicious_cells.push_back({cell->coord.x+1,cell->coord.y+1});
    suspicious_cells.push_back({cell->coord.x+1,cell->coord.y-1});
    suspicious_cells.push_back({cell->coord.x-1,cell->coord.y+1});
    for (auto coord:suspicious_cells){
        if (!(coord.x < 0 or coord.y < 0 or coord.x > 9 or coord.y > 9)){
            if (getCell(coord, tf)->status == CellStatus::Empty)
                getCell(coord, tf)->status = cst;
        }
    }
}

void Field::updateRadar(ShotReturn sr, Coordinates coordinates) {
    Cell* cell = getCell(coordinates, TypeField::Radar);
    setResult(sr, cell);
}

void Field::setResult(ShotReturn sr, Cell *cell) {
    if (sr.shotRes == ShotRes::Bad) return;
    if (sr.shotRes == ShotRes::Miss)
        cell->status = CellStatus::Touched;
    if (sr.shotRes == ShotRes::Kill){
        cell->status = CellStatus::Killed;
        setCellsAround(sr.ship,CellStatus::Touched,TypeField::Radar);
    }
    if (sr.shotRes == ShotRes::Get){
        cell->status = CellStatus::Killed;
        acceptGetTouched(cell->coord, TypeField::Radar);
    }
}

CellStatus Field::getCellStatus(Coordinates coord, TypeField tf) const {
    return getCell(coord, tf)->status;
}

IGameView *Field::getGameView() const {
    return view;
}

void Field::acceptGetTouched(Coordinates coord, TypeField tf) {
    vector<Coordinates> suspicious_cells;
    suspicious_cells.push_back({coord.x-1,coord.y-1});
    suspicious_cells.push_back({coord.x+1,coord.y+1});
    suspicious_cells.push_back({coord.x+1,coord.y-1});
    suspicious_cells.push_back({coord.x-1,coord.y+1});
    for (auto coord:suspicious_cells){
        if (!(coord.x < 0 or coord.y < 0 or coord.x > 9 or coord.y > 9)){
            if (tf == TypeField::Radar)
                (*radar)[coord.x][coord.y]->status = CellStatus::Touched;
            if (tf == TypeField::ourField)
                (*ourField)[coord.x][coord.y]->status = CellStatus::Touched;
        }
    }
}





