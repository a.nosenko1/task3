//
// Created by Vatson on 017 17.01.2021.
//

#include "GamerOptimal.h"

int GamerOptimal::getCountKills() const {
    return countKills;
}

string GamerOptimal::getName() const {
    return name;
}

Field *GamerOptimal::getField() const {
    return field;
}

GamerOptimal::GamerOptimal(string name, IGameView *view) {
    this->view = view;
    this->name = std::move(name);
    auto* newField = new Field(view);
    field = newField;
    countKills = 0;
    cleverRadar = new vector<vector<int>>;
    for (int i = 0; i < 10; ++i) {
        auto* temp = new vector<int>;
        for (int j = 0; j < 10; ++j) {
            temp->push_back(1);
        }
        cleverRadar->push_back(*temp);
    }
    coordDiagonalShot = new vector<Coordinates>; // инициализация для тактической стрельбы
    setDiagonalCoordinates(coordDiagonalShot);
    isShipDestroying = false;
    ships = new vector<int>;
    ships->push_back(1);
    ships->push_back(1);
    ships->push_back(1);
    ships->push_back(1);
    ships->push_back(2);
    ships->push_back(2);
    ships->push_back(2);
    ships->push_back(3);
    ships->push_back(3);
    ships->push_back(4);
    countWins = 0;
};

ShotReturn GamerOptimal::receiveShot(Coordinates coordinates) {
    ShotReturn result = field->receiveShot(coordinates);
    if (result.shotRes == ShotRes::Kill){
        for (int i = 0; i < ships->size(); ++i) {
            if ((*ships)[i] == result.ship->coordinates.size()){
                ships->erase(ships->begin() + i);
                ships->shrink_to_fit();
            }
        }
    }
    return result;
}

void GamerOptimal::setupShips() {
    int typeSetup = rand() % 2 + 1;
    if (typeSetup == 1)
        setupShipsType(TypesOptimalSetup::Left);
    if (typeSetup == 2)
        setupShipsType(TypesOptimalSetup::LeftAndRight);
    view->printMessage(this->getName() + ": ships are located.\n");
    pause(DELAY_TIME);
}

void GamerOptimal::setupShipsType(TypesOptimalSetup type) {
    if (type == TypesOptimalSetup::Left){
        Ship ship;
        ship.coordinates.push_back({0,0});
        ship.coordinates.push_back({1,0});
        ship.coordinates.push_back({2,0});
        ship.coordinates.push_back({3,0});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({0,2});
        ship.coordinates.push_back({1,2});
        ship.coordinates.push_back({2,2});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({4,2});
        ship.coordinates.push_back({5,2});
        ship.coordinates.push_back({6,2});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({8,2});
        ship.coordinates.push_back({9,2});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({8,0});
        ship.coordinates.push_back({9,0});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({5,0});
        ship.coordinates.push_back({6,0});
        field->setupShip(ship);
        ship.coordinates.clear();

        int countFour = 0;
        while (countFour != 4){
            ship.coordinates.push_back({rand() % 6 + 4,rand() % 10});
            if (!field->setupShip(ship)){
                ship.coordinates.clear();
            } else countFour++;
        }
    }
    if (type == TypesOptimalSetup::LeftAndRight){
        Ship ship;
        ship.coordinates.push_back({0,0});
        ship.coordinates.push_back({1,0});
        ship.coordinates.push_back({2,0});
        ship.coordinates.push_back({3,0});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({8,0});
        ship.coordinates.push_back({9,0});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({5,0});
        ship.coordinates.push_back({6,0});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({0,9});
        ship.coordinates.push_back({1,9});
        ship.coordinates.push_back({2,9});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({4,9});
        ship.coordinates.push_back({5,9});
        field->setupShip(ship);
        ship.coordinates.clear();

        ship.coordinates.push_back({7,9});
        ship.coordinates.push_back({8,9});
        ship.coordinates.push_back({9,9});
        field->setupShip(ship);
        ship.coordinates.clear();

        int countFour = 0;
        while (countFour != 4){
            ship.coordinates.push_back({rand() % 5 + 2,rand() % 10});
            if (!field->setupShip(ship)){
                ship.coordinates.clear();
            } else countFour++;
        }
    }
}

bool GamerOptimal::shot(IGamer *opponent) {
    ShotReturn result{};
    Coordinates coord{};
    for(;;){
        coord = whereShot();
        result = opponent->receiveShot(coord);
        if (result.shotRes == ShotRes::Bad) (*cleverRadar)[coord.x][coord.y] = 0;
        if (result.shotRes != ShotRes::Bad) break;
    }
    field->updateRadar(result, coord);
    if (result.shotRes == ShotRes::Kill) {
        countKills++;
        (*cleverRadar)[coord.x][coord.y] = 0;
        setSquareZero({coord.x,coord.y});
        view->putResultMessage(this->getName() + "! You destroyed an opponent ship.\n");
        view->putMessageToBuffer("Opponent killed our ship.\n");
        isShipDestroying = false;
    }
    if (result.shotRes == ShotRes::Get) {
        (*cleverRadar)[coord.x][coord.y] = 0;
        acceptGet(coord);
        isShipDestroying = true;
        view->putResultMessage(this->getName() + "! Hit to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        view->putMessageToBuffer("Opponent hit to " + intToStr(coord.x) + to_string(coord.y) + "\n");
    }
    if (result.shotRes == ShotRes::Miss){
        (*cleverRadar)[coord.x][coord.y] = 0;
        view->putMessageToBuffer("Opponent missed to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        view->putResultMessage(this->getName() + "! Unfortunately, you missed to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        pause(DELAY_TIME);
        return false;
    }
    else {
        pause(DELAY_TIME);
        return true;
    }
}

Coordinates GamerOptimal::findHeavyCoordToShot() const {
    int max = 0;
    int x = -1, y = -1;
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            if ((*cleverRadar)[i][j] > max){
                x = i;
                y = j;
                max = (*cleverRadar)[i][j];
            }
        }
    }
    if (x == -1){
        x = rand() % 10;
        y = rand() % 10;
    }
    (*cleverRadar)[x][y] = 0;
    return Coordinates{x,y};
}

Coordinates GamerOptimal::whereShot() const {
    if (isShipDestroying){
        return findHeavyCoordToShot();
    }
    if (isFourShipsLive()){ // сначала ищем 4-палубники, по диагоналям
        Coordinates coord = coordDiagonalShot->back();
        if (!coordDiagonalShot->empty()){
            coordDiagonalShot->pop_back();
            (*cleverRadar)[coord.x][coord.y] = 0;
            return coord;
        } else return findHeavyCoordToShot();
    }
    if (isFourShipsLive()){ // далее рандомная стрельба
        return findHeavyCoordToShot();
    }
    return findHeavyCoordToShot();
}

void GamerOptimal::setDiagonalCoordinates(vector<Coordinates>* v) {
    coordDiagonalShot->push_back({3,0});
    coordDiagonalShot->push_back({7,0});
    coordDiagonalShot->push_back({2,1});
    coordDiagonalShot->push_back({6,1});
    coordDiagonalShot->push_back({1,2});
    coordDiagonalShot->push_back({5,2});
    coordDiagonalShot->push_back({9,2});
    coordDiagonalShot->push_back({0,3});
    coordDiagonalShot->push_back({4,3});
    coordDiagonalShot->push_back({8,3});
    coordDiagonalShot->push_back({3,4});
    coordDiagonalShot->push_back({7,4});
    coordDiagonalShot->push_back({2,5});
    coordDiagonalShot->push_back({6,5});
    coordDiagonalShot->push_back({1,6});
    coordDiagonalShot->push_back({5,6});
    coordDiagonalShot->push_back({9,6});
    coordDiagonalShot->push_back({0,7});
    coordDiagonalShot->push_back({4,7});
    coordDiagonalShot->push_back({8,7});
    coordDiagonalShot->push_back({3,8});
    coordDiagonalShot->push_back({7,8});
    coordDiagonalShot->push_back({2,9});
    coordDiagonalShot->push_back({6,9});
    reverse(coordDiagonalShot->begin(), coordDiagonalShot->end());
}

bool GamerOptimal::isFourShipsLive() const {
    for (int ship : *ships) {
        if (ship == 4 or ship == 3)
            return true;
    }
    return false;
}

void GamerOptimal::setSquareZero(Coordinates coord) {
    vector<Coordinates> suspicious_cells;
    suspicious_cells.push_back({coord.x+1,coord.y});
    suspicious_cells.push_back({coord.x-1,coord.y});
    suspicious_cells.push_back({coord.x,coord.y+1});
    suspicious_cells.push_back({coord.x,coord.y-1});
    suspicious_cells.push_back({coord.x-1,coord.y-1});
    suspicious_cells.push_back({coord.x+1,coord.y+1});
    suspicious_cells.push_back({coord.x+1,coord.y-1});
    suspicious_cells.push_back({coord.x-1,coord.y+1});
    for (auto coord:suspicious_cells){
        if (!(coord.x < 0 or coord.y < 0 or coord.x > 9 or coord.y > 9)){
            (*cleverRadar)[coord.x][coord.y] = 0;
        }
    }
}

void GamerOptimal::acceptGet(Coordinates coord) {
    vector<Coordinates> suspicious_cells;
    suspicious_cells.push_back({coord.x+1,coord.y});
    suspicious_cells.push_back({coord.x-1,coord.y});
    suspicious_cells.push_back({coord.x,coord.y+1});
    suspicious_cells.push_back({coord.x,coord.y-1});
    for (auto coord:suspicious_cells){
        if (!(coord.x < 0 or coord.y < 0 or coord.x > 9 or coord.y > 9)){
            (*cleverRadar)[coord.x][coord.y] *= 10;
        }
    }
    suspicious_cells.clear();
    suspicious_cells.push_back({coord.x-1,coord.y-1});
    suspicious_cells.push_back({coord.x+1,coord.y+1});
    suspicious_cells.push_back({coord.x+1,coord.y-1});
    suspicious_cells.push_back({coord.x-1,coord.y+1});
    for (auto coord:suspicious_cells){
        if (!(coord.x < 0 or coord.y < 0 or coord.x > 9 or coord.y > 9)){
            (*cleverRadar)[coord.x][coord.y] = 0;
        }
    }
}

PlayerType GamerOptimal::getPlayerType() const {
    return pt;
}

GamerOptimal::~GamerOptimal() {
    vector<vector<int>>().swap(*cleverRadar);
    delete coordDiagonalShot;
    delete ships;
    delete field;
}




