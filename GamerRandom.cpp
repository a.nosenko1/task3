//
// Created by Vatson on 018 18.01.2021.
//

#include "GamerRandom.h"

GamerRandom::GamerRandom(string name, IGameView *view) {
    this->view = view;
    this->name = std::move(name);
    auto* newField = new Field(view);
    field = newField;
    countKills = 0;
    countWins = 0;
}

void GamerRandom::setupShips() {
    for (int length = 1; length < 5; ++length) {
        for (int j = 0; j < 5-length; ++j) {
            label1:
            int first = rand() % 10;
            int second = rand() % 10;
            int d = rand() % 2;
            char direction;
            if (d == 0) direction = 'h';
            else direction = 'v';
            Ship ship{};
            if (direction == 'h'){
                for (int i = 0; i < length; ++i) {
                    ship.coordinates.push_back({first+i,second});
                }
            }
            else if (direction == 'v'){
                for (int i = 0; i < length; ++i) {
                    ship.coordinates.push_back({first,second-i});
                }
            }
            if (!field->setupShip(ship)){
                goto label1;
            }
        }
    }
    view->draw(field);
    view->printMessage(this->getName() + ": ships are located.\n");
    pause(DELAY_TIME);
}

bool GamerRandom::shot(IGamer *opponent) {
    ShotReturn result{};
    Coordinates coord{};
    for(;;){
        coord = whereShot();
        result = opponent->receiveShot(coord);
        if (result.shotRes != ShotRes::Bad) break;
    }
    field->updateRadar(result, coord);
    if (result.shotRes == ShotRes::Kill) {
        countKills++;
        view->putMessageToBuffer("Opponent killed our ship.\n");
        view->putResultMessage(this->getName() + "! You destroyed an opponent ship.\n");
    }
    if (result.shotRes == ShotRes::Get) {
        view->putMessageToBuffer("Opponent hit to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        view->putResultMessage(this->getName() + "! Hit to " + intToStr(coord.x) + to_string(coord.y) + "\n");
    }
    if (result.shotRes == ShotRes::Miss){
        view->putMessageToBuffer("Opponent missed to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        view->putResultMessage(this->getName() + "! Unfortunately, you missed to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        pause(DELAY_TIME);
        return false;
    }
    else {
        pause(DELAY_TIME);
        return true;
    }
}

Coordinates GamerRandom::whereShot() {
    return Coordinates{rand() % 10,rand() % 10};
}

ShotReturn GamerRandom::receiveShot(Coordinates coordinates) {
    ShotReturn result = field->receiveShot(coordinates);
    return result;
}

int GamerRandom::getCountKills() const {
    return countKills;
}

string GamerRandom::getName() const {
    return name;
}

Field *GamerRandom::getField() const {
    return field;
}

PlayerType GamerRandom::getPlayerType() const {
    return PlayerType::RandomGamer;
}

GamerRandom::~GamerRandom() {
    delete field;
}


