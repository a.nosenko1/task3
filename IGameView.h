//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_IGAMEVIEW_H
#define TASK3_IGAMEVIEW_H
#include "includes.h"
class Field;

class IGameView {
public:
    virtual void draw(Field* field) = 0;
    virtual ~IGameView() = default;
    virtual void printMessage(const string& message) = 0;

    virtual void putMessageToBuffer(const string& message) = 0;
    virtual void showStackMessages() = 0;
    virtual void showLastBufferMessage() = 0;
    virtual void clearBuffer() = 0;

    virtual void showResultMessage() = 0;
    virtual void putResultMessage(const string& message) = 0;
};


#endif //TASK3_IGAMEVIEW_H
