//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_CONSOLEVIEW_H
#define TASK3_CONSOLEVIEW_H
#include "includes.h"
#include "IGameView.h"


class ConsoleView: public IGameView {
public:
    void draw(Field* field) override;
    ~ConsoleView() override = default;
    void printMessage(const string& message) override;
    void putMessageToBuffer(const string& message) override;
    void showStackMessages() override;
    void showLastBufferMessage() override;
    void clearBuffer() override;
    void showResultMessage() override;
    void putResultMessage(const string& message) override;
private:
    vector<string> bufer;
    string result;
};


#endif //TASK3_CONSOLEVIEW_H
