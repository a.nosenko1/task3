cmake_minimum_required(VERSION 3.17)
project(task3)

set(CMAKE_CXX_STANDARD 14)

add_executable(task3 main.cpp Game.cpp Game.h IGamer.h Field.cpp Field.h IGameView.h ConsoleView.cpp ConsoleView.h GamerUser.cpp GamerUser.h includes.h PlayersFactory.h GamerOptimal.cpp GamerOptimal.h GamerRandom.cpp GamerRandom.h Parser.h)

