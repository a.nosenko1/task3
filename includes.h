//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_INCLUDES_H
#define TASK3_INCLUDES_H
using namespace std;
#define DELAY_TIME 1 //в секундах
#include <string>
#include <ctime>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iostream>
#include "optionparser.h"

// -h -c 1 -f random -s random/user/optimal
// --help --count --first --second
enum class PlayerType{
    ConsoleGamer = 0,
    RandomGamer = 1,
    OptimalGamer = 2
};

enum class ShotRes{
    Bad = -1,
    Miss = 0,
    Get = 1,
    Kill = 2
};
struct Coordinates{
    int x;
    int y;
    Coordinates(){
        x = -1;
        y = -1;
    }
    Coordinates(int x, int y){
        this->x = x;
        this->y = y;
    }
    bool operator==(Coordinates coord) const{
        if ((coord.x != this->x) or (coord.y != this->y))
            return false;
        return true;
    };
    bool operator!=(Coordinates coord) const {
        if (coord == *this) return false;
        return true;
    }
};

enum class GameStatus{
    inGame = 0,
    gameOver = 1,
};
enum class CellStatus{
    Empty = 0,
    Ship = 1,
    Touched = 2,
    Killed = 3
};

struct Cell{
    Coordinates coord;
    CellStatus status;
    Cell(){
        coord = *(new Coordinates());
        status = CellStatus::Empty;
    }
};
struct Ship{
    vector<Coordinates> coordinates;
    vector<Cell*> cells;
    bool operator==(Ship ship) {
        if (ship.coordinates.size() == this->coordinates.size()){
            for (int i = 0; i < ship.coordinates.size(); ++i) {
                if (ship.coordinates[i] != this->coordinates[i])
                    return false;
            }
        } else return false;
        return true;
    }
};

struct ShotReturn{
    ShotRes shotRes;
    Ship* ship;
};
enum class TypeField{
    Radar = 0,
    ourField = 1,
};

enum class GameMod{
    userVSbot = 0,
    botVSbot = 1,
    userVSuser = 2,
};
string intToStr(int x);
void pause(unsigned int delay);
#endif //TASK3_INCLUDES_H
