//
// Created by Vatson on 005 05.01.2021.
//

#include "ConsoleView.h"
#include "Field.h"

void ConsoleView::draw(Field* field) {
    system("clear");
    printf("          Field:\n");
    cout << "   0 1 2 3 4 5 6 7 8 9" << endl;
    for (int x = 0; x < 10; ++x) {
        printf(" %c ", 'A'+x);
        for (int y = 0; y < 10; ++y){
            if (field->getCellStatus({x,y}, TypeField::ourField) == CellStatus::Empty)
                printf("  ");
            if (field->getCellStatus({x,y}, TypeField::ourField) == CellStatus::Ship)
                printf("# ");
            if (field->getCellStatus({x,y}, TypeField::ourField) == CellStatus::Touched)
                printf("* ");
            if (field->getCellStatus({x,y}, TypeField::ourField) == CellStatus::Killed)
                printf("X ");
        }
        printf("\n");
    }
    printf("          Radar:\n");
    cout << "   0 1 2 3 4 5 6 7 8 9" << endl;
    for (int x = 0; x < 10; ++x) {
        printf(" %c ", 'A'+x);
        for (int y = 0; y < 10; ++y){
            if (field->getCellStatus({x,y}, TypeField::Radar) == CellStatus::Empty)
                printf("  ");
            if (field->getCellStatus({x,y}, TypeField::Radar) == CellStatus::Ship)
                printf("# ");
            if (field->getCellStatus({x,y}, TypeField::Radar) == CellStatus::Touched)
                printf("* ");
            if (field->getCellStatus({x,y}, TypeField::Radar) == CellStatus::Killed)
                printf("X ");
        }
        printf("\n");
    }
}

void ConsoleView::printMessage(const string &message) {
    cout << message << '\n';
}

void ConsoleView::putMessageToBuffer(const string &message) {
    bufer.push_back(message);
}

void ConsoleView::showStackMessages() {
//    reverse(bufer.begin(),bufer.end());
//    for (int i = 0; i < bufer.size(); ++i) {
//        cout << bufer.back();
//        bufer.pop_back();
//    }
    for (auto it = bufer.begin(); it != bufer.end(); ++it) {
        std::cout << *it;
    }
    bufer.clear();
}

void ConsoleView::showLastBufferMessage() {
    if (!bufer.empty()){
        cout << bufer.back();
        bufer.pop_back();
    }
}

void ConsoleView::clearBuffer() {
    bufer.clear();
}

void ConsoleView::putResultMessage(const string &message) {
    result = message;
}

void ConsoleView::showResultMessage() {
    if (!result.empty())
        cout << result;
}


