//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_GAME_H
#define TASK3_GAME_H
#include "includes.h"
#include "Parser.h"
#include "IGamer.h"
#include "IGameView.h"

class Game {
public:
    explicit Game(Parameters parameters);
    void startGame();

private:
    GameStatus gameStatus = GameStatus::inGame;
    IGamer* currentPlayer;
    IGamer* nextPlayer;
    Parameters parameters;
    IGameView* view;
    bool isPlayersSwitched;
    int wins[2];

    void setupPlayersShips();
    void switchPlayers();
    void restart();
    void showCurrentScore();
};


#endif //TASK3_GAME_H
