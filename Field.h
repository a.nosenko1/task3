//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_FIELD_H
#define TASK3_FIELD_H
#include "includes.h"
#include "IGameView.h"
class Field {
public:
    explicit Field(IGameView* view);
    bool setupShip(Ship ship); //установить корабль, и проверять, можно ли вообще так корабль установить
    ShotReturn receiveShot(Coordinates coordinates); //принять выстрел по координатам, вернуть результат
    void updateRadar(ShotReturn sr, Coordinates coordinates); /*обновить радар в соответствии с
                                                                полученным результатом выстрела*/
    void draw() const; //отрисовать поле в IGameView
    CellStatus getCellStatus(Coordinates coord, TypeField tf) const; // для отрисовки
    IGameView* getGameView() const;
private:
    vector<Ship>* ships;
    vector<vector<Cell*>>* ourField;
    vector<vector<Cell*>>* radar;
    IGameView* view;

    Cell* getCell(Coordinates coordinates, TypeField tf) const; //достать указатель на ячеку по координатам(из радара или поля)
    ShotReturn makeShot(Cell* cell); //принять выстрел в ячейку, вернуть результат(реализация receiveShot)
    void setResult(ShotReturn sr, Cell* cell); //реализация updateRadar
    Ship* whatShip(Cell* cell) const; //вернуть указатель на корабль в этой ячейке,или вернуть nullpointer
    bool isShipAlive(Ship* ship) const; //чекает все ячейки на killed
    void setCellsAround(Ship* ship, CellStatus cs, TypeField tf); //окружить корабль touched ячейками(не заменяя другие)
    bool isShipAround(Cell* cs, TypeField tf) const; //проверить квадрат, для проверки возможности установки корабля
    bool isSetupShipPossible(Coordinates coordinates, TypeField tf=TypeField::ourField) const; // есть ли рядом корабли
    void setCellsSquare(Cell* cell, CellStatus cst, TypeField tf);
    void acceptGetTouched(Coordinates coord, TypeField tf); //сверху, снизу, слева, справа

};


#endif //TASK3_FIELD_H
