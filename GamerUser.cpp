//
// Created by Vatson on 005 05.01.2021.
//

#include "GamerUser.h"

bool GamerUser::shot(IGamer *opponent) {
    ShotReturn result{};
    Coordinates coord{};
    for(;;){
        coord = takeCoordinates();
        result = opponent->receiveShot(coord);
        if (result.shotRes == ShotRes::Bad)
            view->printMessage(this->getName() + "! Try to shot again.");
        if (result.shotRes != ShotRes::Bad) break;
    }
    field->updateRadar(result, coord);
    if (result.shotRes == ShotRes::Kill) {
        countKills++;
        view->putMessageToBuffer("Opponent destroyed our ship.\n");
        view->putResultMessage(this->getName() + "! You destroyed an opponent ship.\n");
        return true;
    }
    if (result.shotRes == ShotRes::Miss){
        view->putMessageToBuffer("Opponent missed to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        view->putResultMessage(this->getName() + "! Unfortunately, you missed to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        return false;
    }
    if (result.shotRes == ShotRes::Get){
        view->putMessageToBuffer("Opponent hit to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        view->putResultMessage(this->getName() + "! Hit to " + intToStr(coord.x) + to_string(coord.y) + "\n");
        return true;
    }
    else return true;
}

Coordinates GamerUser::takeCoordinates() const {
    while (true){
        view->printMessage("Print coordinates to shoot:");
        string input, word;
        cin >> input;
        cout << endl;
        if (input.size() != 2){
            view->printMessage("Bad input(count), try again:");
            continue;
        }
        char letter = input[0];
        bool isGoodLetter = false;
        for (char i = 'A'; i < 'J'+1; ++i) {
            if (letter == i) isGoodLetter = true;
        }
        if (!isGoodLetter){
            view->printMessage("Bad input(GoodLetter), try again:");
            continue;
        }
        isGoodLetter = false;
        string num(&input[1]);
        int number = stoi(num);
        for (int i = 0; i < 10; ++i) {
            if (number == i) isGoodLetter = true;
        }
        if (!isGoodLetter){
            view->printMessage("Bad input(GoodNumber), try again:");
            continue;
        }
        return Coordinates{letter-'A',number};
    }

}

void GamerUser::setupShips() {
    view->printMessage("How to setup? (R - random/O - optimally/M - manually):");
    string input;
    cin >> input;
    if (input == "0") optimalSetup();
    if (input == "R") randomSetup();
    else manualSetup();
    view->printMessage(this->getName() + ": ships are located.\n");
    pause(DELAY_TIME);
}

GamerUser::GamerUser(string name, IGameView* view) {
    this->view = view;
    this->name = std::move(name);
    auto* newField = new Field(view);
    field = newField;
    countKills = 0;
    countWins = 0;
}

ShotReturn GamerUser::receiveShot(Coordinates coordinates) {
    ShotReturn result = field->receiveShot(coordinates);
    return result;
}

int GamerUser::getCountKills() const {
    return countKills;
}

string GamerUser::getName() const {
    return name;
}

Field *GamerUser::getField() const {
    return field;
}

void GamerUser::optimalSetup() {
    Ship ship;
    ship.coordinates.emplace_back(0, 0);
    ship.coordinates.emplace_back(1, 0);
    ship.coordinates.emplace_back(2, 0);
    ship.coordinates.emplace_back(3, 0);
    field->setupShip(ship);
    ship.coordinates.clear();

    ship.coordinates.emplace_back(8, 0);
    ship.coordinates.emplace_back(9, 0);
    field->setupShip(ship);
    ship.coordinates.clear();

    ship.coordinates.emplace_back(5, 0);
    ship.coordinates.emplace_back(6, 0);
    field->setupShip(ship);
    ship.coordinates.clear();

    ship.coordinates.emplace_back(0, 9);
    ship.coordinates.emplace_back(1, 9);
    ship.coordinates.emplace_back(2, 9);
    field->setupShip(ship);
    ship.coordinates.clear();

    ship.coordinates.emplace_back(4, 9);
    ship.coordinates.emplace_back(5, 9);
    field->setupShip(ship);
    ship.coordinates.clear();

    ship.coordinates.emplace_back(7, 9);
    ship.coordinates.emplace_back(8, 9);
    ship.coordinates.emplace_back(9, 9);
    field->setupShip(ship);
    ship.coordinates.clear();

    int countFour = 0;
    while (countFour != 4) {
        ship.coordinates.emplace_back(rand() % 5 + 2, rand() % 10);
        if (!field->setupShip(ship)) {
            ship.coordinates.clear();
        } else countFour++;
    }
}

void GamerUser::randomSetup() {
    for (int length = 1; length < 5; ++length) {
        for (int j = 0; j < 5-length; ++j) {
            while(true){
                int first = rand() % 10;
                int second = rand() % 10;
                int d = rand() % 2;
                char direction;
                if (d == 0) direction = 'h';
                else direction = 'v';
                Ship ship{};
                if (direction == 'h'){
                    for (int i = 0; i < length; ++i) {
                        ship.coordinates.emplace_back(first+i,second);
                    }
                }
                else {
                    for (int i = 0; i < length; ++i) {
                        ship.coordinates.emplace_back(first, second - i);
                    }
                }
                if (!field->setupShip(ship)){
                    continue;
                }
                break;
            }
        }
    }
    view->draw(field);
}

PlayerType GamerUser::getPlayerType() const {
    return pt;
}

void GamerUser::manualSetup() {
    for (int length = 1; length < 5; ++length) {
        for (int j = 0; j < 5-length; ++j) {
            while(true){
                view->draw(field);
                view->showLastBufferMessage(); // если была ошибка
                view->printMessage("Let's setup a ship with length = " + to_string(length));
                view->printMessage("Print the first coordinate of ship, direction (horizontal(h) or vertical(v)):"); //D4 h
                string input;
                cin >> input;
                if (input.size() != 2){
                    view->putMessageToBuffer("Bad input(count), try again:\n");
                    continue;
                }
                char letter = input[0];
                bool isGoodLetter = false;
                for (char i = 'A'; i < 'J'+ 1; ++i) {
                    if (letter == i) isGoodLetter = true;
                }
                if (!isGoodLetter){
                    view->putMessageToBuffer("Bad input(BadLetter), try again:\n");
                    continue;
                }
                isGoodLetter = false;
                string num(&input[1]);
                int number = stoi(num);
                for (int i = 0; i < 10; ++i) {
                    if (number == i) isGoodLetter = true;
                }
                if (!isGoodLetter){
                    view->putMessageToBuffer("Bad input(BadNumber), try again:\n");
                    continue;
                }
                Coordinates coord = {letter-'A',number-1};
                cin >> input;
                char direction = input[0];
                Ship ship{};
                if (direction == 'h'){
                    for (int i = 0; i < length; ++i) {
                        ship.coordinates.emplace_back(letter-'A'+i,number);
                    }
                }
                else if (direction == 'v'){
                    for (int i = 0; i < length; ++i) {
                        ship.coordinates.emplace_back(letter-'A',number-i);
                    }
                }
                else throw runtime_error("cin error");
                if (!field->setupShip(ship)){
                    view->putMessageToBuffer("Impossible to setup ship here, try another coord\n");
                    continue;
                }
                break;
            }
        }
    }
}

GamerUser::~GamerUser() {
    delete field;
};
