//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_GAMERUSER_H
#define TASK3_GAMERUSER_H
#include "includes.h"
#include "IGamer.h"

class GamerUser: public IGamer {
public:
    explicit GamerUser(string name, IGameView* view);
    ~GamerUser() override;
    void setupShips() override;
    bool shot(IGamer* opponent) override;
    ShotReturn receiveShot(Coordinates coordinates) override;

    int getCountKills() const override;
    string getName() const override;
    Field* getField() const override;
    PlayerType getPlayerType() const;

private:
    Field* field;
    int countKills;
    IGameView* view;
    PlayerType pt = PlayerType::ConsoleGamer;
    int countWins; // кол-во побед

    void optimalSetup();
    void randomSetup();
    void manualSetup();
    Coordinates takeCoordinates() const;
};


#endif //TASK3_GAMERUSER_H
