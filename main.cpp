#include <iostream>
#include <ctime>
#include "includes.h"
#include "Parser.h"
#include "Game.h"


string intToStr(int x) {
    const char a = (x + 'A');
    string s(&a, 1);
    return s;
}
void pause(unsigned int delay) {
    time_t start, end;
    double d;
    start = time(nullptr);
    while (true) {
        end = time(nullptr);
        d = difftime(end, start);
        if (d >= delay) break;
    }
}
using namespace std;
int main(int argc, char** argv) {
    srand((time(nullptr)));

    argc -= (argc > 0); argv += (argc > 0);
    option::Stats stats(usage, argc, argv);
    option::Option options[stats.options_max], buffer[stats.buffer_max];
    option::Parser parse(usage, argc, argv, options, buffer);
    if (parse.error())
        return 0;
    if (options[HELP]) {
        option::printUsage(cout, usage);
        return 0;
    }
    if (parse.nonOptionsCount() > 0){
        cout << "There are non-Options Arguments: " << endl;
        for (int i = 0; i < parse.nonOptionsCount(); ++i){
            cout << "Non-option #" << i << ": " << parse.nonOption(i) << "\n";
        }
        return 0;
    }

    Parameters p(options);
    Game game(p);
    game.startGame();

    return 0;
}

