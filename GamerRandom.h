//
// Created by Vatson on 018 18.01.2021.
//

#ifndef TASK3_GAMERRANDOM_H
#define TASK3_GAMERRANDOM_H
#include "IGamer.h"
#include "includes.h"

class GamerRandom: public IGamer {
public:
    explicit GamerRandom(string name, IGameView* view);
    ~GamerRandom() override;
    void setupShips() override;
    bool shot(IGamer* opponent) override;
    ShotReturn receiveShot(Coordinates coordinates) override;

    int getCountKills() const override;
    string getName() const override;
    Field* getField() const override;
    PlayerType getPlayerType() const override;

private:
    Field* field;
    int countKills;
    IGameView* view;
    const PlayerType pt = PlayerType::RandomGamer;
    int countWins;

    static Coordinates whereShot() ;
};


#endif //TASK3_GAMERRANDOM_H
