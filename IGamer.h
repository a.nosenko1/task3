//
// Created by Vatson on 005 05.01.2021.
//

#ifndef TASK3_IGAMER_H
#define TASK3_IGAMER_H
#include "includes.h"
#include "Field.h"

class IGamer {
public:
    virtual ~IGamer()= default;;
    virtual void setupShips() = 0;
    virtual bool shot(IGamer* opponent) = 0;
    virtual ShotReturn receiveShot(Coordinates coordinates) = 0;
    virtual int getCountKills() const = 0;
    virtual string getName() const = 0;
    virtual Field* getField() const = 0;
    virtual PlayerType getPlayerType() const = 0;

private:
    int countKills;
    int countWins; // кол-во побед
protected:
    string name;
};



#endif //TASK3_IGAMER_H
